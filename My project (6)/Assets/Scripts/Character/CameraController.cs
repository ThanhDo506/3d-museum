using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform cameraPosition;
    private Transform cameraTranform;

    private void Start()
    {
        cameraTranform = GetComponent<Transform>();
    }

    void Update()
    {
        cameraTranform.position = cameraPosition.position;
    }
}
