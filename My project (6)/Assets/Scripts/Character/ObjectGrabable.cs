using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGrabable : MonoBehaviour
{
    private Rigidbody _rigidbody;

    private Transform grabPointTranform;
    private Collider _collider;
    private Transform parentTranform;

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _collider = GetComponentInChildren<Collider>();
        parentTranform = GetComponent<Transform>();
    }

    public void Grab(Transform objectGrabPointTranform)
    {
        grabPointTranform = objectGrabPointTranform;
        _rigidbody.useGravity = false;
        _rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
        // _rigidbody.freezeRotation = true;
        _collider.isTrigger = true;
    }

    public void Drop()
    {
        grabPointTranform = null;
        _rigidbody.useGravity = true;
        _rigidbody.interpolation = RigidbodyInterpolation.None;
        // _rigidbody.freezeRotation = false;
        _collider.isTrigger = false;
    }

    private void FixedUpdate()
    {
        if (grabPointTranform != null)
        {
            Vector3 newPosition = Vector3.Lerp(transform.position, grabPointTranform.position, Time.deltaTime * 2.0f);
            _rigidbody.MovePosition(newPosition);
            // parentTranform.position = grabPointTranform.position;
        }
    }
    
}
