using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    [SerializeField] private GameObject menu;
    [SerializeField] private GameObject player;
    public void BackToMenu()
    {
        SceneManager.LoadScene("menuScreen");
    }

    public void Resume()
    {
        player.GetComponent<PlayerMovement>().isControllable = true;
        Cursor.lockState = CursorLockMode.Locked;
        menu.SetActive(false);
    }

    public void ResetPosition()
    {
        player.transform.position = new Vector3(17f, 1f, -1f);
        player.GetComponent<PlayerMovement>().isControllable = true;
        Cursor.lockState = CursorLockMode.Locked;
        menu.SetActive(false);
    }
}
