using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MyButton : MonoBehaviour
{
    public AudioClip audioClip;
    private AudioSource audioSource;
    [SerializeField] private GameObject text; 

    private void Start()
    {
        if ((audioSource = GetComponent<AudioSource>()) != null)
        {
            audioSource.Stop();
        }

        if (text != null)
        {
            text.SetActive(false);
        }
    }

    public void DoSt()
    {
        if (audioSource != null)
        {
            if (audioSource.isPlaying)
            {
                audioSource.Stop();
            }
            else
            {
                audioSource.Play();
            }
        }
        if (text != null)
        {
            if (text.activeSelf)
            {
                Debug.Log("Show");
                text.SetActive(false);
            }
            else
            {
                Debug.Log("disable");
                text.SetActive(true);
            }
        }
    }
}
