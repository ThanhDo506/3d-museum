using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Movement")] private float moveSpeed;
    public float groundDrag;
    public float walkSpeed;
    public float runSpeed;

    public float jumpForce;
    public float jumpCooldown;
    public float airMultiplier;

    [Header("Ground Check")] public float playerHeight;
    public LayerMask whatIsGround;
    private bool grounded;
    private bool canJump = true;

    public Transform orientation;

    private float horizontalInput;
    private float verticalInput;

    private Vector3 moveDirection;

    private Rigidbody _rigidbody;

    public bool isControllable = true;
    [SerializeField] private GameObject menuScreen;

    [Header("GUI MANAGER")] [SerializeField]
    private Transform camTranform;
    [SerializeField] private GameObject takeText;

    [SerializeField] private GameObject dropText;
    [SerializeField] private GameObject FText;
    [SerializeField] private GameObject RGHText;
    
    
    [Header("Cursor")]
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    void OnMouseEnter()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
        Cursor.visible = true;
    }

    void OnMouseExit()
    {
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
        Cursor.visible = false;
    }
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.freezeRotation = true;
        moveSpeed = walkSpeed;
    }

    private void Update()
    {
        GUICheck();

        grounded = Physics.Raycast(transform.position, Vector3.down, playerHeight * 0.5f + 0.2f, whatIsGround);
        input();
        SpeedControl();
        if (grounded)
        {
            _rigidbody.drag = groundDrag;
        }
        else
        {
            _rigidbody.drag = 0;
        }
    }

    private void FixedUpdate()
    {
        MovePlayer();
    }

    private void input()
    {
        if (isControllable)
        {
            horizontalInput = Input.GetAxisRaw("Horizontal");
            verticalInput = Input.GetAxisRaw("Vertical");
            if (Input.GetButtonDown("Jump") && canJump && grounded)
            {
                canJump = false;
                Jump();
                Invoke(nameof(ResetJump), jumpCooldown);
            }

            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                moveSpeed = runSpeed;
            }
            else if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                moveSpeed = walkSpeed;
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isControllable)
            {
                isControllable = false;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.Confined;
                menuScreen.SetActive(true);
            }
            else
            {
                menuScreen.SetActive(false);
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                isControllable = true;
            }
        }
    }

    private void MovePlayer()
    {
        moveDirection = orientation.forward * verticalInput + orientation.right * horizontalInput;
        if (grounded)
            _rigidbody.AddForce(moveDirection.normalized * moveSpeed * 10f, ForceMode.Force);
        else
        {
            _rigidbody.AddForce(moveDirection.normalized * moveSpeed * 10f * airMultiplier, ForceMode.Force);
        }
        }

    private void SpeedControl()
    {
        Vector3 flatVel = new Vector3(_rigidbody.velocity.x, 0f, _rigidbody.velocity.z);

        if (flatVel.magnitude > moveSpeed)
        {
            Vector3 limitedVel = flatVel.normalized * moveSpeed;
            _rigidbody.velocity = new Vector3(limitedVel.x, _rigidbody.velocity.y, limitedVel.z);
        }
    }

    private void Jump()
    {
        _rigidbody.velocity = new Vector3(_rigidbody.velocity.x, 0f, _rigidbody.velocity.z);
        _rigidbody.AddForce(transform.up * jumpForce, ForceMode.Impulse);
    }

    private void ResetJump()
    {
        canJump = true;
    }

    private void GUICheck()
    {
        if (Physics.Raycast(camTranform.position,
                camTranform.forward,
                out RaycastHit raycastHit, 4.0f
            ))
        {
            int layerHit = raycastHit.transform.gameObject.layer;
            if (layerHit == LayerMask.NameToLayer("pickUpAble") && GetComponent<PlayerPickUpAndDrop>()._objectGrabable == null)
            {
                takeText.SetActive(true);
                FText.SetActive(false);
                dropText.SetActive(false);
                RGHText.SetActive(false);
                OnMouseEnter();
                    // takeText.SetActive(false);
                    // FText.SetActive(false);
                    // dropText.SetActive(true);
                    // RGHText.SetActive(true);
            } else if (layerHit == LayerMask.NameToLayer("clickAble"))
            {
                takeText.SetActive(false);
                FText.SetActive(true);
                dropText.SetActive(false);
                RGHText.SetActive(false);
                OnMouseEnter();
            }
            // } else if (GetComponent<PlayerPickUpAndDrop>()._objectGrabable != null)
            // {
            //     dropText.SetActive(true);
            //     RGHText.SetActive(true);
            //     FText.SetActive(false);
            //     takeText.SetActive(false);
            // }
        }
        else
        {
            dropText.SetActive(false);
            RGHText.SetActive(false);
            FText.SetActive(false);
            takeText.SetActive(false);
            OnMouseExit();
        }
    }
}
