﻿namespace UnityTemplateProjects.Character
{
    public interface ActionBtn
    {
        public void OnClick();
    }
}