using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;


public class ClickButton : MonoBehaviour
{ 
    [SerializeField] private Transform playerCameraTranform;
    [SerializeField] private LayerMask ObjectCanClickLayer;
    private MyButton _myButton;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (Physics.Raycast(playerCameraTranform.transform.position, 
                    playerCameraTranform.forward,
                    out RaycastHit raycastHit, 4.0f,
                    ObjectCanClickLayer))
            {
                _myButton = raycastHit.transform.GetComponentInChildren<MyButton>();
                if (_myButton != null)
                {
                    _myButton.DoSt();
                    _myButton = null;
                }
            }
        }
    }
}
