using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuBtn : MonoBehaviour
{
    public void start()
    {
        SceneManager.LoadScene("Museum");
    }

    public void exitProgram()
    {
        Application.Quit();
    }
}
