using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPickUpAndDrop : MonoBehaviour
{
    [SerializeField] private LayerMask ObjectCanPickupLayer;
    [SerializeField] private Transform playerCameraTranform;
    [SerializeField] private Transform objectGrabPointTranform;

    public ObjectGrabable _objectGrabable;
    public KeyCode pickUpAndDropKey = KeyCode.E;

    public float pickupDistance = 2.0f;
    void Update()
    {
        if (Input.GetKeyDown(pickUpAndDropKey))
        {
            if (_objectGrabable == null)
            {
                if (Physics.Raycast(playerCameraTranform.transform.position, 
                        playerCameraTranform.forward,
                        out RaycastHit raycastHit, pickupDistance,
                        ObjectCanPickupLayer))
                {
                    _objectGrabable = raycastHit.transform.GetComponentInChildren<ObjectGrabable>();
                    if (_objectGrabable != null)
                    {
                        Debug.Log("Can controll obj");
                        _objectGrabable.Grab(objectGrabPointTranform);
                    }
                }
            }
            else
            {
                _objectGrabable.Drop();
                _objectGrabable = null;
            }
        }

        if (_objectGrabable != null)
        {
            if (Input.GetKey(KeyCode.R))
            {
                _objectGrabable.GetComponent<Transform>().rotation = Quaternion.Euler(0,0,0);
            }

            if (Input.GetKey(KeyCode.G))
            {
                _objectGrabable.GetComponent<Transform>().Rotate(0, .5f, 0);
            }

            if (Input.GetKey(KeyCode.H))
            {
                _objectGrabable.GetComponent<Transform>().Rotate(0, 0, 0.5f);
            }
        }
    }
}
