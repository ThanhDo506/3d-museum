using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    public AudioClip clip;
    AudioSource audioSrc;

    void Start()
    {
        audioSrc = GetComponent<AudioSource>();
    }

    public void ToggleSound()
    {
        if (audioSrc.isPlaying)
        {
            audioSrc.Stop();
        }
        else
        {
            audioSrc.PlayOneShot(clip);
        }
    }
}
